<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="mil" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="yes" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<attributes/>
<variantdefs/>
<libraries>
<library name="common">
<packages>
<package name="HDR-1x4">
<pad name="1" x="-3.81" y="0" drill="0.9" shape="square" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="2" x="-1.27" y="0" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="3" x="1.27" y="0" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="4" x="3.81" y="0" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<wire layer="21" width="0.25" x1="5.08" y1="-1.27" x2="5.08" y2="1.27"/>
<wire layer="21" width="0.25" x1="-5.08" y1="-1.27" x2="5.08" y2="-1.27"/>
<wire layer="21" width="0.25" x1="-5.08" y1="1.27" x2="5.08" y2="1.27"/>
<wire layer="21" width="0.25" x1="-5.08" y1="-1.27" x2="-5.08" y2="1.27"/>
</package>
<package name="HDR-2x5">
<pad name="1" x="-5.08" y="-1.27" drill="0.9" shape="square" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="2" x="-5.08" y="1.27" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="3" x="-2.54" y="-1.27" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="4" x="-2.54" y="1.27" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="5" x="0" y="-1.27" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="6" x="0" y="1.27" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="7" x="2.54" y="-1.27" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="8" x="2.54" y="1.27" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="9" x="5.08" y="-1.27" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="10" x="5.08" y="1.27" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<wire layer="21" width="0.25" x1="-6.35" y1="-2.54" x2="6.35" y2="-2.54"/>
<wire layer="21" width="0.25" x1="-6.35" y1="2.54" x2="6.35" y2="2.54"/>
<wire layer="21" width="0.25" x1="-6.35" y1="-2.54" x2="-6.35" y2="2.54"/>
<wire layer="21" width="0.25" x1="6.35" y1="-2.54" x2="6.35" y2="2.54"/>
</package>
<package name="HDR-2x4">
<pad name="1" x="-3.81" y="-1.27" drill="0.9" shape="square" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="2" x="-3.81" y="1.27" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="3" x="-1.27" y="-1.27" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="4" x="-1.27" y="1.27" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="5" x="1.27" y="-1.27" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="6" x="1.27" y="1.27" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="7" x="3.81" y="-1.27" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="8" x="3.81" y="1.27" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<wire layer="21" width="0.25" x1="-5.08" y1="-2.54" x2="5.08" y2="-2.54"/>
<wire layer="21" width="0.25" x1="-5.08" y1="2.54" x2="5.08" y2="2.54"/>
<wire layer="21" width="0.25" x1="-5.08" y1="-2.54" x2="-5.08" y2="2.54"/>
<wire layer="21" width="0.25" x1="5.08" y1="-2.54" x2="5.08" y2="2.54"/>
</package>
<package name="HDR-1x2">
<pad name="1" x="-1.27" y="0" drill="0.9" shape="square" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="2" x="1.27" y="0" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<wire layer="21" width="0.25" x1="2.54" y1="-1.27" x2="2.54" y2="1.27"/>
<wire layer="21" width="0.25" x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27"/>
<wire layer="21" width="0.25" x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27"/>
<wire layer="21" width="0.25" x1="-2.54" y1="1.27" x2="2.54" y2="1.27"/>
</package>
<package name="HDR-1x18">
<pad name="1" x="-21.59" y="0" drill="0.9" shape="square" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="2" x="-19.05" y="0" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="3" x="-16.51" y="0" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="4" x="-13.97" y="0" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="5" x="-11.43" y="0" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="6" x="-8.89" y="0" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="7" x="-6.35" y="0" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="8" x="-3.81" y="0" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="9" x="-1.27" y="0" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="10" x="1.27" y="0" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="11" x="3.81" y="0" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="12" x="6.35" y="0" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="13" x="8.89" y="0" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="14" x="11.43" y="0" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="15" x="13.97" y="0" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="16" x="16.51" y="0" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="17" x="19.05" y="0" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="18" x="21.59" y="0" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<wire layer="21" width="0.25" x1="-20.94" y1="1.27" x2="22.86" y2="1.27"/>
<wire layer="21" width="0.25" x1="22.86" y1="-1.27" x2="22.86" y2="1.27"/>
<wire layer="21" width="0.25" x1="-22.86" y1="-1.27" x2="22.86" y2="-1.27"/>
<wire layer="21" width="0.25" x1="-22.86" y1="-1.27" x2="-22.86" y2="1.27"/>
<wire layer="21" width="0.25" x1="-22.86" y1="1.27" x2="-20.94" y2="1.27"/>
</package>
<package name="HDR-1x3">
<pad name="1" x="-2.54" y="0" drill="0.9" shape="square" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="2" x="0" y="0" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<pad name="3" x="2.54" y="0" drill="0.9" diameter="1.5" rot="R0" stop="yes" thermals="no"/>
<wire layer="21" width="0.25" x1="-3.81" y1="-1.27" x2="-3.81" y2="1.27"/>
<wire layer="21" width="0.25" x1="-3.81" y1="1.27" x2="3.81" y2="1.27"/>
<wire layer="21" width="0.25" x1="3.81" y1="-1.27" x2="3.81" y2="1.27"/>
<wire layer="21" width="0.25" x1="-3.81" y1="-1.27" x2="3.81" y2="-1.27"/>
</package>
<package name="RES-7.62/4x1.5">
<pad name="2" x="3.81" y="0" drill="0.559" diameter="1.27" rot="R0" stop="yes" thermals="no"/>
<pad name="1" x="-3.81" y="0" drill="0.559" diameter="1.27" rot="R0" stop="yes" thermals="no"/>
<wire layer="21" width="0.25" x1="2.036" y1="-0.762" x2="2.036" y2="0.762"/>
<wire layer="21" width="0.25" x1="-2.036" y1="-0.762" x2="2.036" y2="-0.762"/>
<wire layer="21" width="0.25" x1="-2.036" y1="-0.762" x2="-2.036" y2="0.762"/>
<wire layer="21" width="0.25" x1="-2.036" y1="0.762" x2="2.036" y2="0.762"/>
</package>
</packages>
<symbols>
<symbol name="HDR-1X4">
<wire layer="94" width="0.25" x1="-0.635" y1="3.81" x2="0.635" y2="5.08"/>
<wire layer="94" width="0.25" x1="-0.635" y1="3.81" x2="0.635" y2="2.54"/>
<wire layer="94" width="0.25" x1="-0.635" y1="3.81" x2="5.08" y2="3.81"/>
<wire layer="94" width="0.25" x1="-0.635" y1="1.27" x2="0.635" y2="2.54"/>
<wire layer="94" width="0.25" x1="-0.635" y1="1.27" x2="0.635" y2="0"/>
<wire layer="94" width="0.25" x1="-0.635" y1="1.27" x2="5.08" y2="1.27"/>
<wire layer="94" width="0.25" x1="-5.08" y1="5.08" x2="5.08" y2="5.08"/>
<wire layer="94" width="0.25" x1="5.08" y1="5.08" x2="5.08" y2="-5.08"/>
<wire layer="94" width="0.25" x1="5.08" y1="-5.08" x2="-5.08" y2="-5.08"/>
<wire layer="94" width="0.25" x1="-5.08" y1="-5.08" x2="-5.08" y2="5.08"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-1.27" x2="0.635" y2="0"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-1.27" x2="0.635" y2="-2.54"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-1.27" x2="5.08" y2="-1.27"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-3.81" x2="0.635" y2="-2.54"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-3.81" x2="0.635" y2="-5.08"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-3.81" x2="5.08" y2="-3.81"/>
<pin name="P1" visible="pad" length="short" direction="pas" rot="R180" x="7.62" y="3.81"/>
<pin name="P2" visible="pad" length="short" direction="pas" rot="R180" x="7.62" y="1.27"/>
<pin name="P3" visible="pad" length="short" direction="pas" rot="R180" x="7.62" y="-1.27"/>
<pin name="P4" visible="pad" length="short" direction="pas" rot="R180" x="7.62" y="-3.81"/>
</symbol>
<symbol name="HDR-2X5">
<wire layer="94" width="0.25" x1="-0.635" y1="11.43" x2="0.635" y2="12.7"/>
<wire layer="94" width="0.25" x1="-0.635" y1="11.43" x2="0.635" y2="10.16"/>
<wire layer="94" width="0.25" x1="-0.635" y1="11.43" x2="5.08" y2="11.43"/>
<wire layer="94" width="0.25" x1="-0.635" y1="8.89" x2="0.635" y2="10.16"/>
<wire layer="94" width="0.25" x1="-0.635" y1="8.89" x2="0.635" y2="7.62"/>
<wire layer="94" width="0.25" x1="-0.635" y1="8.89" x2="5.08" y2="8.89"/>
<wire layer="94" width="0.25" x1="-5.08" y1="12.7" x2="5.08" y2="12.7"/>
<wire layer="94" width="0.25" x1="5.08" y1="12.7" x2="5.08" y2="-12.7"/>
<wire layer="94" width="0.25" x1="5.08" y1="-12.7" x2="-5.08" y2="-12.7"/>
<wire layer="94" width="0.25" x1="-5.08" y1="-12.7" x2="-5.08" y2="12.7"/>
<wire layer="94" width="0.25" x1="-0.635" y1="6.35" x2="0.635" y2="7.62"/>
<wire layer="94" width="0.25" x1="-0.635" y1="6.35" x2="0.635" y2="5.08"/>
<wire layer="94" width="0.25" x1="-0.635" y1="6.35" x2="5.08" y2="6.35"/>
<wire layer="94" width="0.25" x1="-0.635" y1="3.81" x2="0.635" y2="5.08"/>
<wire layer="94" width="0.25" x1="-0.635" y1="3.81" x2="0.635" y2="2.54"/>
<wire layer="94" width="0.25" x1="-0.635" y1="3.81" x2="5.08" y2="3.81"/>
<wire layer="94" width="0.25" x1="-0.635" y1="1.27" x2="0.635" y2="2.54"/>
<wire layer="94" width="0.25" x1="-0.635" y1="1.27" x2="0.635" y2="0"/>
<wire layer="94" width="0.25" x1="-0.635" y1="1.27" x2="5.08" y2="1.27"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-1.27" x2="0.635" y2="0"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-1.27" x2="0.635" y2="-2.54"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-1.27" x2="5.08" y2="-1.27"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-3.81" x2="0.635" y2="-2.54"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-3.81" x2="0.635" y2="-5.08"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-3.81" x2="5.08" y2="-3.81"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-6.35" x2="0.635" y2="-5.08"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-6.35" x2="0.635" y2="-7.62"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-6.35" x2="5.08" y2="-6.35"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-8.89" x2="0.635" y2="-7.62"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-8.89" x2="0.635" y2="-10.16"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-8.89" x2="5.08" y2="-8.89"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-11.43" x2="0.635" y2="-10.16"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-11.43" x2="0.635" y2="-12.7"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-11.43" x2="5.08" y2="-11.43"/>
<pin name="P1" visible="pad" length="short" direction="pas" rot="R180" x="7.62" y="11.43"/>
<pin name="P2" visible="pad" length="short" direction="pas" rot="R180" x="7.62" y="8.89"/>
<pin name="P3" visible="pad" length="short" direction="pas" rot="R180" x="7.62" y="6.35"/>
<pin name="P4" visible="pad" length="short" direction="pas" rot="R180" x="7.62" y="3.81"/>
<pin name="P5" visible="pad" length="short" direction="pas" rot="R180" x="7.62" y="1.27"/>
<pin name="P6" visible="pad" length="short" direction="pas" rot="R180" x="7.62" y="-1.27"/>
<pin name="P7" visible="pad" length="short" direction="pas" rot="R180" x="7.62" y="-3.81"/>
<pin name="P8" visible="pad" length="short" direction="pas" rot="R180" x="7.62" y="-6.35"/>
<pin name="P9" visible="pad" length="short" direction="pas" rot="R180" x="7.62" y="-8.89"/>
<pin name="P10" visible="pad" length="short" direction="pas" rot="R180" x="7.62" y="-11.43"/>
</symbol>
<symbol name="HDR-2X5_2_0">
<wire layer="94" width="0.25" x1="0.635" y1="11.43" x2="-0.635" y2="12.7"/>
<wire layer="94" width="0.25" x1="0.635" y1="11.43" x2="-0.635" y2="10.16"/>
<wire layer="94" width="0.25" x1="0.635" y1="11.43" x2="-5.08" y2="11.43"/>
<wire layer="94" width="0.25" x1="0.635" y1="8.89" x2="-0.635" y2="10.16"/>
<wire layer="94" width="0.25" x1="0.635" y1="8.89" x2="-0.635" y2="7.62"/>
<wire layer="94" width="0.25" x1="0.635" y1="8.89" x2="-5.08" y2="8.89"/>
<wire layer="94" width="0.25" x1="-5.08" y1="12.7" x2="5.08" y2="12.7"/>
<wire layer="94" width="0.25" x1="5.08" y1="12.7" x2="5.08" y2="-12.7"/>
<wire layer="94" width="0.25" x1="5.08" y1="-12.7" x2="-5.08" y2="-12.7"/>
<wire layer="94" width="0.25" x1="-5.08" y1="-12.7" x2="-5.08" y2="12.7"/>
<wire layer="94" width="0.25" x1="0.635" y1="6.35" x2="-0.635" y2="7.62"/>
<wire layer="94" width="0.25" x1="0.635" y1="6.35" x2="-0.635" y2="5.08"/>
<wire layer="94" width="0.25" x1="0.635" y1="6.35" x2="-5.08" y2="6.35"/>
<wire layer="94" width="0.25" x1="0.635" y1="3.81" x2="-0.635" y2="5.08"/>
<wire layer="94" width="0.25" x1="0.635" y1="3.81" x2="-0.635" y2="2.54"/>
<wire layer="94" width="0.25" x1="0.635" y1="3.81" x2="-5.08" y2="3.81"/>
<wire layer="94" width="0.25" x1="0.635" y1="1.27" x2="-0.635" y2="2.54"/>
<wire layer="94" width="0.25" x1="0.635" y1="1.27" x2="-0.635" y2="0"/>
<wire layer="94" width="0.25" x1="0.635" y1="1.27" x2="-5.08" y2="1.27"/>
<wire layer="94" width="0.25" x1="0.635" y1="-1.27" x2="-0.635" y2="0"/>
<wire layer="94" width="0.25" x1="0.635" y1="-1.27" x2="-0.635" y2="-2.54"/>
<wire layer="94" width="0.25" x1="0.635" y1="-1.27" x2="-5.08" y2="-1.27"/>
<wire layer="94" width="0.25" x1="0.635" y1="-3.81" x2="-0.635" y2="-2.54"/>
<wire layer="94" width="0.25" x1="0.635" y1="-3.81" x2="-0.635" y2="-5.08"/>
<wire layer="94" width="0.25" x1="0.635" y1="-3.81" x2="-5.08" y2="-3.81"/>
<wire layer="94" width="0.25" x1="0.635" y1="-6.35" x2="-0.635" y2="-5.08"/>
<wire layer="94" width="0.25" x1="0.635" y1="-6.35" x2="-0.635" y2="-7.62"/>
<wire layer="94" width="0.25" x1="0.635" y1="-6.35" x2="-5.08" y2="-6.35"/>
<wire layer="94" width="0.25" x1="0.635" y1="-8.89" x2="-0.635" y2="-7.62"/>
<wire layer="94" width="0.25" x1="0.635" y1="-8.89" x2="-0.635" y2="-10.16"/>
<wire layer="94" width="0.25" x1="0.635" y1="-8.89" x2="-5.08" y2="-8.89"/>
<wire layer="94" width="0.25" x1="0.635" y1="-11.43" x2="-0.635" y2="-10.16"/>
<wire layer="94" width="0.25" x1="0.635" y1="-11.43" x2="-0.635" y2="-12.7"/>
<wire layer="94" width="0.25" x1="0.635" y1="-11.43" x2="-5.08" y2="-11.43"/>
<pin name="P1" visible="pad" length="short" direction="pas" x="-7.62" y="11.43"/>
<pin name="P2" visible="pad" length="short" direction="pas" x="-7.62" y="8.89"/>
<pin name="P3" visible="pad" length="short" direction="pas" x="-7.62" y="6.35"/>
<pin name="P4" visible="pad" length="short" direction="pas" x="-7.62" y="3.81"/>
<pin name="P5" visible="pad" length="short" direction="pas" x="-7.62" y="1.27"/>
<pin name="P6" visible="pad" length="short" direction="pas" x="-7.62" y="-1.27"/>
<pin name="P7" visible="pad" length="short" direction="pas" x="-7.62" y="-3.81"/>
<pin name="P8" visible="pad" length="short" direction="pas" x="-7.62" y="-6.35"/>
<pin name="P9" visible="pad" length="short" direction="pas" x="-7.62" y="-8.89"/>
<pin name="P10" visible="pad" length="short" direction="pas" x="-7.62" y="-11.43"/>
</symbol>
<symbol name="HDR-2X4">
<wire layer="94" width="0.25" x1="0.635" y1="8.89" x2="-0.635" y2="10.16"/>
<wire layer="94" width="0.25" x1="0.635" y1="8.89" x2="-0.635" y2="7.62"/>
<wire layer="94" width="0.25" x1="0.635" y1="8.89" x2="-5.08" y2="8.89"/>
<wire layer="94" width="0.25" x1="0.635" y1="6.35" x2="-0.635" y2="7.62"/>
<wire layer="94" width="0.25" x1="0.635" y1="6.35" x2="-0.635" y2="5.08"/>
<wire layer="94" width="0.25" x1="0.635" y1="6.35" x2="-5.08" y2="6.35"/>
<wire layer="94" width="0.25" x1="-5.08" y1="10.16" x2="5.08" y2="10.16"/>
<wire layer="94" width="0.25" x1="5.08" y1="10.16" x2="5.08" y2="-10.16"/>
<wire layer="94" width="0.25" x1="5.08" y1="-10.16" x2="-5.08" y2="-10.16"/>
<wire layer="94" width="0.25" x1="-5.08" y1="-10.16" x2="-5.08" y2="10.16"/>
<wire layer="94" width="0.25" x1="0.635" y1="3.81" x2="-0.635" y2="5.08"/>
<wire layer="94" width="0.25" x1="0.635" y1="3.81" x2="-0.635" y2="2.54"/>
<wire layer="94" width="0.25" x1="0.635" y1="3.81" x2="-5.08" y2="3.81"/>
<wire layer="94" width="0.25" x1="0.635" y1="1.27" x2="-0.635" y2="2.54"/>
<wire layer="94" width="0.25" x1="0.635" y1="1.27" x2="-0.635" y2="0"/>
<wire layer="94" width="0.25" x1="0.635" y1="1.27" x2="-5.08" y2="1.27"/>
<wire layer="94" width="0.25" x1="0.635" y1="-1.27" x2="-0.635" y2="0"/>
<wire layer="94" width="0.25" x1="0.635" y1="-1.27" x2="-0.635" y2="-2.54"/>
<wire layer="94" width="0.25" x1="0.635" y1="-1.27" x2="-5.08" y2="-1.27"/>
<wire layer="94" width="0.25" x1="0.635" y1="-3.81" x2="-0.635" y2="-2.54"/>
<wire layer="94" width="0.25" x1="0.635" y1="-3.81" x2="-0.635" y2="-5.08"/>
<wire layer="94" width="0.25" x1="0.635" y1="-3.81" x2="-5.08" y2="-3.81"/>
<wire layer="94" width="0.25" x1="0.635" y1="-6.35" x2="-0.635" y2="-5.08"/>
<wire layer="94" width="0.25" x1="0.635" y1="-6.35" x2="-0.635" y2="-7.62"/>
<wire layer="94" width="0.25" x1="0.635" y1="-6.35" x2="-5.08" y2="-6.35"/>
<wire layer="94" width="0.25" x1="0.635" y1="-8.89" x2="-0.635" y2="-7.62"/>
<wire layer="94" width="0.25" x1="0.635" y1="-8.89" x2="-0.635" y2="-10.16"/>
<wire layer="94" width="0.25" x1="0.635" y1="-8.89" x2="-5.08" y2="-8.89"/>
<pin name="P1" visible="pad" length="short" direction="pas" x="-7.62" y="8.89"/>
<pin name="P2" visible="pad" length="short" direction="pas" x="-7.62" y="6.35"/>
<pin name="P3" visible="pad" length="short" direction="pas" x="-7.62" y="3.81"/>
<pin name="P4" visible="pad" length="short" direction="pas" x="-7.62" y="1.27"/>
<pin name="P5" visible="pad" length="short" direction="pas" x="-7.62" y="-1.27"/>
<pin name="P6" visible="pad" length="short" direction="pas" x="-7.62" y="-3.81"/>
<pin name="P7" visible="pad" length="short" direction="pas" x="-7.62" y="-6.35"/>
<pin name="P8" visible="pad" length="short" direction="pas" x="-7.62" y="-8.89"/>
</symbol>
<symbol name="HDR-1X2">
<wire layer="94" width="0.25" x1="0.635" y1="1.27" x2="-0.635" y2="2.54"/>
<wire layer="94" width="0.25" x1="0.635" y1="1.27" x2="-0.635" y2="0"/>
<wire layer="94" width="0.25" x1="0.635" y1="1.27" x2="-5.08" y2="1.27"/>
<wire layer="94" width="0.25" x1="0.635" y1="-1.27" x2="-0.635" y2="0"/>
<wire layer="94" width="0.25" x1="0.635" y1="-1.27" x2="-0.635" y2="-2.54"/>
<wire layer="94" width="0.25" x1="0.635" y1="-1.27" x2="-5.08" y2="-1.27"/>
<wire layer="94" width="0.25" x1="5.08" y1="-2.54" x2="5.08" y2="2.54"/>
<wire layer="94" width="0.25" x1="5.08" y1="2.54" x2="-5.08" y2="2.54"/>
<wire layer="94" width="0.25" x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54"/>
<wire layer="94" width="0.25" x1="-5.08" y1="-2.54" x2="5.08" y2="-2.54"/>
<pin name="P1" visible="pad" length="short" direction="pas" x="-7.62" y="1.27"/>
<pin name="P2" visible="pad" length="short" direction="pas" x="-7.62" y="-1.27"/>
</symbol>
<symbol name="HDR-1X2_5_0">
<wire layer="94" width="0.25" x1="-0.635" y1="1.27" x2="0.635" y2="2.54"/>
<wire layer="94" width="0.25" x1="-0.635" y1="1.27" x2="0.635" y2="0"/>
<wire layer="94" width="0.25" x1="-0.635" y1="1.27" x2="5.08" y2="1.27"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-1.27" x2="0.635" y2="0"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-1.27" x2="0.635" y2="-2.54"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-1.27" x2="5.08" y2="-1.27"/>
<wire layer="94" width="0.25" x1="-5.08" y1="-2.54" x2="-5.08" y2="2.54"/>
<wire layer="94" width="0.25" x1="-5.08" y1="2.54" x2="5.08" y2="2.54"/>
<wire layer="94" width="0.25" x1="5.08" y1="2.54" x2="5.08" y2="-2.54"/>
<wire layer="94" width="0.25" x1="5.08" y1="-2.54" x2="-5.08" y2="-2.54"/>
<pin name="P1" visible="pad" length="short" direction="pas" rot="R180" x="7.62" y="1.27"/>
<pin name="P2" visible="pad" length="short" direction="pas" rot="R180" x="7.62" y="-1.27"/>
</symbol>
<symbol name="HDR-1X18">
<wire layer="94" width="0.25" x1="0.635" y1="21.59" x2="-0.635" y2="22.86"/>
<wire layer="94" width="0.25" x1="0.635" y1="21.59" x2="-0.635" y2="20.32"/>
<wire layer="94" width="0.25" x1="0.635" y1="21.59" x2="-5.08" y2="21.59"/>
<wire layer="94" width="0.25" x1="0.635" y1="19.05" x2="-0.635" y2="20.32"/>
<wire layer="94" width="0.25" x1="0.635" y1="19.05" x2="-0.635" y2="17.78"/>
<wire layer="94" width="0.25" x1="0.635" y1="19.05" x2="-5.08" y2="19.05"/>
<wire layer="94" width="0.25" x1="-5.08" y1="22.86" x2="5.08" y2="22.86"/>
<wire layer="94" width="0.25" x1="5.08" y1="22.86" x2="5.08" y2="-22.86"/>
<wire layer="94" width="0.25" x1="5.08" y1="-22.86" x2="-5.08" y2="-22.86"/>
<wire layer="94" width="0.25" x1="-5.08" y1="-22.86" x2="-5.08" y2="22.86"/>
<wire layer="94" width="0.25" x1="0.635" y1="16.51" x2="-0.635" y2="17.78"/>
<wire layer="94" width="0.25" x1="0.635" y1="16.51" x2="-0.635" y2="15.24"/>
<wire layer="94" width="0.25" x1="0.635" y1="16.51" x2="-5.08" y2="16.51"/>
<wire layer="94" width="0.25" x1="0.635" y1="13.97" x2="-0.635" y2="15.24"/>
<wire layer="94" width="0.25" x1="0.635" y1="13.97" x2="-0.635" y2="12.7"/>
<wire layer="94" width="0.25" x1="0.635" y1="13.97" x2="-5.08" y2="13.97"/>
<wire layer="94" width="0.25" x1="0.635" y1="11.43" x2="-0.635" y2="12.7"/>
<wire layer="94" width="0.25" x1="0.635" y1="11.43" x2="-0.635" y2="10.16"/>
<wire layer="94" width="0.25" x1="0.635" y1="11.43" x2="-5.08" y2="11.43"/>
<wire layer="94" width="0.25" x1="0.635" y1="8.89" x2="-0.635" y2="10.16"/>
<wire layer="94" width="0.25" x1="0.635" y1="8.89" x2="-0.635" y2="7.62"/>
<wire layer="94" width="0.25" x1="0.635" y1="8.89" x2="-5.08" y2="8.89"/>
<wire layer="94" width="0.25" x1="0.635" y1="6.35" x2="-0.635" y2="7.62"/>
<wire layer="94" width="0.25" x1="0.635" y1="6.35" x2="-0.635" y2="5.08"/>
<wire layer="94" width="0.25" x1="0.635" y1="6.35" x2="-5.08" y2="6.35"/>
<wire layer="94" width="0.25" x1="0.635" y1="3.81" x2="-0.635" y2="5.08"/>
<wire layer="94" width="0.25" x1="0.635" y1="3.81" x2="-0.635" y2="2.54"/>
<wire layer="94" width="0.25" x1="0.635" y1="3.81" x2="-5.08" y2="3.81"/>
<wire layer="94" width="0.25" x1="0.635" y1="1.27" x2="-0.635" y2="2.54"/>
<wire layer="94" width="0.25" x1="0.635" y1="1.27" x2="-0.635" y2="0"/>
<wire layer="94" width="0.25" x1="0.635" y1="1.27" x2="-5.08" y2="1.27"/>
<wire layer="94" width="0.25" x1="0.635" y1="-1.27" x2="-0.635" y2="0"/>
<wire layer="94" width="0.25" x1="0.635" y1="-1.27" x2="-0.635" y2="-2.54"/>
<wire layer="94" width="0.25" x1="0.635" y1="-1.27" x2="-5.08" y2="-1.27"/>
<wire layer="94" width="0.25" x1="0.635" y1="-3.81" x2="-0.635" y2="-2.54"/>
<wire layer="94" width="0.25" x1="0.635" y1="-3.81" x2="-0.635" y2="-5.08"/>
<wire layer="94" width="0.25" x1="0.635" y1="-3.81" x2="-5.08" y2="-3.81"/>
<wire layer="94" width="0.25" x1="0.635" y1="-6.35" x2="-0.635" y2="-5.08"/>
<wire layer="94" width="0.25" x1="0.635" y1="-6.35" x2="-0.635" y2="-7.62"/>
<wire layer="94" width="0.25" x1="0.635" y1="-6.35" x2="-5.08" y2="-6.35"/>
<wire layer="94" width="0.25" x1="0.635" y1="-8.89" x2="-0.635" y2="-7.62"/>
<wire layer="94" width="0.25" x1="0.635" y1="-8.89" x2="-0.635" y2="-10.16"/>
<wire layer="94" width="0.25" x1="0.635" y1="-8.89" x2="-5.08" y2="-8.89"/>
<wire layer="94" width="0.25" x1="0.635" y1="-11.43" x2="-0.635" y2="-10.16"/>
<wire layer="94" width="0.25" x1="0.635" y1="-11.43" x2="-0.635" y2="-12.7"/>
<wire layer="94" width="0.25" x1="0.635" y1="-11.43" x2="-5.08" y2="-11.43"/>
<wire layer="94" width="0.25" x1="0.635" y1="-13.97" x2="-0.635" y2="-12.7"/>
<wire layer="94" width="0.25" x1="0.635" y1="-13.97" x2="-0.635" y2="-15.24"/>
<wire layer="94" width="0.25" x1="0.635" y1="-13.97" x2="-5.08" y2="-13.97"/>
<wire layer="94" width="0.25" x1="0.635" y1="-16.51" x2="-0.635" y2="-15.24"/>
<wire layer="94" width="0.25" x1="0.635" y1="-16.51" x2="-0.635" y2="-17.78"/>
<wire layer="94" width="0.25" x1="0.635" y1="-16.51" x2="-5.08" y2="-16.51"/>
<wire layer="94" width="0.25" x1="0.635" y1="-19.05" x2="-0.635" y2="-17.78"/>
<wire layer="94" width="0.25" x1="0.635" y1="-19.05" x2="-0.635" y2="-20.32"/>
<wire layer="94" width="0.25" x1="0.635" y1="-19.05" x2="-5.08" y2="-19.05"/>
<wire layer="94" width="0.25" x1="0.635" y1="-21.59" x2="-0.635" y2="-20.32"/>
<wire layer="94" width="0.25" x1="0.635" y1="-21.59" x2="-0.635" y2="-22.86"/>
<wire layer="94" width="0.25" x1="0.635" y1="-21.59" x2="-5.08" y2="-21.59"/>
<pin name="P1" visible="pad" length="short" direction="pas" x="-7.62" y="21.59"/>
<pin name="P2" visible="pad" length="short" direction="pas" x="-7.62" y="19.05"/>
<pin name="P3" visible="pad" length="short" direction="pas" x="-7.62" y="16.51"/>
<pin name="P4" visible="pad" length="short" direction="pas" x="-7.62" y="13.97"/>
<pin name="P5" visible="pad" length="short" direction="pas" x="-7.62" y="11.43"/>
<pin name="P6" visible="pad" length="short" direction="pas" x="-7.62" y="8.89"/>
<pin name="P7" visible="pad" length="short" direction="pas" x="-7.62" y="6.35"/>
<pin name="P8" visible="pad" length="short" direction="pas" x="-7.62" y="3.81"/>
<pin name="P9" visible="pad" length="short" direction="pas" x="-7.62" y="1.27"/>
<pin name="P10" visible="pad" length="short" direction="pas" x="-7.62" y="-1.27"/>
<pin name="P11" visible="pad" length="short" direction="pas" x="-7.62" y="-3.81"/>
<pin name="P12" visible="pad" length="short" direction="pas" x="-7.62" y="-6.35"/>
<pin name="P13" visible="pad" length="short" direction="pas" x="-7.62" y="-8.89"/>
<pin name="P14" visible="pad" length="short" direction="pas" x="-7.62" y="-11.43"/>
<pin name="P15" visible="pad" length="short" direction="pas" x="-7.62" y="-13.97"/>
<pin name="P16" visible="pad" length="short" direction="pas" x="-7.62" y="-16.51"/>
<pin name="P17" visible="pad" length="short" direction="pas" x="-7.62" y="-19.05"/>
<pin name="P18" visible="pad" length="short" direction="pas" x="-7.62" y="-21.59"/>
</symbol>
<symbol name="HDR-1X3">
<wire layer="94" width="0.25" x1="-0.635" y1="2.54" x2="0.635" y2="3.81"/>
<wire layer="94" width="0.25" x1="-0.635" y1="2.54" x2="0.635" y2="1.27"/>
<wire layer="94" width="0.25" x1="-0.635" y1="2.54" x2="5.08" y2="2.54"/>
<wire layer="94" width="0.25" x1="-0.635" y1="0" x2="0.635" y2="1.27"/>
<wire layer="94" width="0.25" x1="-0.635" y1="0" x2="0.635" y2="-1.27"/>
<wire layer="94" width="0.25" x1="-0.635" y1="0" x2="5.08" y2="0"/>
<wire layer="94" width="0.25" x1="-5.08" y1="3.81" x2="5.08" y2="3.81"/>
<wire layer="94" width="0.25" x1="5.08" y1="3.81" x2="5.08" y2="-3.81"/>
<wire layer="94" width="0.25" x1="5.08" y1="-3.81" x2="-5.08" y2="-3.81"/>
<wire layer="94" width="0.25" x1="-5.08" y1="-3.81" x2="-5.08" y2="3.81"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-2.54" x2="0.635" y2="-1.27"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-2.54" x2="0.635" y2="-3.81"/>
<wire layer="94" width="0.25" x1="-0.635" y1="-2.54" x2="5.08" y2="-2.54"/>
<pin name="P1" visible="pad" length="short" direction="pas" rot="R180" x="7.62" y="2.54"/>
<pin name="P2" visible="pad" length="short" direction="pas" rot="R180" x="7.62" y="0"/>
<pin name="P3" visible="pad" length="short" direction="pas" rot="R180" x="7.62" y="-2.54"/>
</symbol>
<symbol name="RES">
<wire layer="94" width="0.25" x1="3.175" y1="-1.27" x2="3.81" y2="0"/>
<wire layer="94" width="0.25" x1="1.905" y1="1.27" x2="3.175" y2="-1.27"/>
<wire layer="94" width="0.25" x1="0.635" y1="-1.27" x2="1.905" y2="1.27"/>
<wire layer="94" width="0.25" x1="-0.635" y1="1.27" x2="0.635" y2="-1.27"/>
<wire layer="94" width="0.25" x1="-1.905" y1="-1.27" x2="-0.635" y2="1.27"/>
<wire layer="94" width="0.25" x1="-3.175" y1="1.27" x2="-1.905" y2="-1.27"/>
<wire layer="94" width="0.25" x1="-3.81" y1="0" x2="-3.175" y2="1.27"/>
<pin name="B" visible="pad" length="short" direction="pas" rot="R180" x="6.35" y="0"/>
<pin name="A" visible="pad" length="short" direction="pas" x="-6.35" y="0"/>
</symbol>
<symbol name="GND">
<wire layer="94" width="0.25" x1="-1.905" y1="1.016" x2="1.905" y2="1.016"/>
<wire layer="94" width="0.25" x1="-1.27" y1="0" x2="1.27" y2="0"/>
<wire layer="94" width="0.25" x1="-0.508" y1="-1.016" x2="0.508" y2="-1.016"/>
<pin name="GND" visible="pad" length="short" direction="sup" rot="R270" x="0" y="3.556"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="HDR-1X4" prefix="TMC_X">
<gates>
<gate name="PART_1" symbol="HDR-1X4" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HDR-1x4">
<connects>
<connect gate="PART_1" pin="P1" pad="1"/>
<connect gate="PART_1" pin="P2" pad="2"/>
<connect gate="PART_1" pin="P3" pad="3"/>
<connect gate="PART_1" pin="P4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HDR-2X5" prefix="EXP">
<gates>
<gate name="PART_1" symbol="HDR-2X5" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HDR-2x5">
<connects>
<connect gate="PART_1" pin="P1" pad="1"/>
<connect gate="PART_1" pin="P2" pad="2"/>
<connect gate="PART_1" pin="P3" pad="3"/>
<connect gate="PART_1" pin="P4" pad="4"/>
<connect gate="PART_1" pin="P5" pad="5"/>
<connect gate="PART_1" pin="P6" pad="6"/>
<connect gate="PART_1" pin="P7" pad="7"/>
<connect gate="PART_1" pin="P8" pad="8"/>
<connect gate="PART_1" pin="P9" pad="9"/>
<connect gate="PART_1" pin="P10" pad="10"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HDR-2X5_2" prefix="AUX-">
<gates>
<gate name="PART_1" symbol="HDR-2X5_2_0" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HDR-2x5">
<connects>
<connect gate="PART_1" pin="P1" pad="1"/>
<connect gate="PART_1" pin="P2" pad="2"/>
<connect gate="PART_1" pin="P3" pad="3"/>
<connect gate="PART_1" pin="P4" pad="4"/>
<connect gate="PART_1" pin="P5" pad="5"/>
<connect gate="PART_1" pin="P6" pad="6"/>
<connect gate="PART_1" pin="P7" pad="7"/>
<connect gate="PART_1" pin="P8" pad="8"/>
<connect gate="PART_1" pin="P9" pad="9"/>
<connect gate="PART_1" pin="P10" pad="10"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HDR-2X4" prefix="AUX-">
<gates>
<gate name="PART_1" symbol="HDR-2X4" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HDR-2x4">
<connects>
<connect gate="PART_1" pin="P1" pad="1"/>
<connect gate="PART_1" pin="P2" pad="2"/>
<connect gate="PART_1" pin="P3" pad="3"/>
<connect gate="PART_1" pin="P4" pad="4"/>
<connect gate="PART_1" pin="P5" pad="5"/>
<connect gate="PART_1" pin="P6" pad="6"/>
<connect gate="PART_1" pin="P7" pad="7"/>
<connect gate="PART_1" pin="P8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HDR-1X2" prefix="ZMin">
<gates>
<gate name="PART_1" symbol="HDR-1X2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HDR-1x2">
<connects>
<connect gate="PART_1" pin="P1" pad="1"/>
<connect gate="PART_1" pin="P2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HDR-1X2_5" prefix="12V">
<gates>
<gate name="PART_1" symbol="HDR-1X2_5_0" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HDR-1x2">
<connects>
<connect gate="PART_1" pin="P1" pad="1"/>
<connect gate="PART_1" pin="P2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HDR-1X18" prefix="AUX-">
<gates>
<gate name="PART_1" symbol="HDR-1X18" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HDR-1x18">
<connects>
<connect gate="PART_1" pin="P1" pad="1"/>
<connect gate="PART_1" pin="P2" pad="2"/>
<connect gate="PART_1" pin="P3" pad="3"/>
<connect gate="PART_1" pin="P4" pad="4"/>
<connect gate="PART_1" pin="P5" pad="5"/>
<connect gate="PART_1" pin="P6" pad="6"/>
<connect gate="PART_1" pin="P7" pad="7"/>
<connect gate="PART_1" pin="P8" pad="8"/>
<connect gate="PART_1" pin="P9" pad="9"/>
<connect gate="PART_1" pin="P10" pad="10"/>
<connect gate="PART_1" pin="P11" pad="11"/>
<connect gate="PART_1" pin="P12" pad="12"/>
<connect gate="PART_1" pin="P13" pad="13"/>
<connect gate="PART_1" pin="P14" pad="14"/>
<connect gate="PART_1" pin="P15" pad="15"/>
<connect gate="PART_1" pin="P16" pad="16"/>
<connect gate="PART_1" pin="P17" pad="17"/>
<connect gate="PART_1" pin="P18" pad="18"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HDR-1X3" prefix="ZProbe">
<gates>
<gate name="PART_1" symbol="HDR-1X3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HDR-1x3">
<connects>
<connect gate="PART_1" pin="P1" pad="1"/>
<connect gate="PART_1" pin="P2" pad="2"/>
<connect gate="PART_1" pin="P3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RES" prefix="R">
<gates>
<gate name="PART_1" symbol="RES" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RES-7.62/4x1.5">
<connects>
<connect gate="PART_1" pin="B" pad="2"/>
<connect gate="PART_1" pin="A" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="VALUE" value="20K"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="NetPort">
<gates>
<gate name="PART_1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<connects/>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<classes>
<class number="0" name="Default" width="0" drill="0"/>
<class number="1" name="Supply" width="0" drill="0"/>
</classes>
<parts>
<part name="EXP1" library="common" deviceset="HDR-2X5" device=""/>
<part name="EXP2" library="common" deviceset="HDR-2X5" device=""/>
<part name="AUX-4" library="common" deviceset="HDR-1X18" device=""/>
<part name="AUX-3" library="common" deviceset="HDR-2X4" device=""/>
<part name="AUX-2" library="common" deviceset="HDR-2X5_2" device=""/>
<part name="TMC_X" library="common" deviceset="HDR-1X4" device=""/>
<part name="TMC_Y" library="common" deviceset="HDR-1X4" device=""/>
<part name="TMC_Z0" library="common" deviceset="HDR-1X4" device=""/>
<part name="TMC_E" library="common" deviceset="HDR-1X4" device=""/>
<part name="TMC_Z1" library="common" deviceset="HDR-1X4" device=""/>
<part name="ZProbe" library="common" deviceset="HDR-1X3" device=""/>
<part name="ZMin" library="common" deviceset="HDR-1X2" device=""/>
<part name="R1" library="common" deviceset="RES" device="" value="20K"/>
<part name="R2" library="common" deviceset="RES" device="" value="10K"/>
<part name="12V" library="common" deviceset="HDR-1X2_5" device=""/>
<part name="NetPort1" library="common" deviceset="GND" device=""/>
<part name="NetPort2" library="common" deviceset="GND" device=""/>
<part name="NetPort3" library="common" deviceset="GND" device=""/>
<part name="NetPort4" library="common" deviceset="GND" device=""/>
</parts>
<modules/>
<sheets>
<sheet>
<plain/>
<moduleinsts/>
<instances>
<instance part="EXP1" gate="PART_1" x="-8.89" y="27.94">
<attribute name="NAME" value="EXP1" layer="95" x="-11.273" y="43.153" size="1.628" align="top-left"/>
</instance>
<instance part="EXP2" gate="PART_1" x="-8.89" y="-3.81">
<attribute name="NAME" value="EXP2" layer="95" x="-11.601" y="11.403" size="1.628" align="top-left"/>
</instance>
<instance part="AUX-4" gate="PART_1" x="102.87" y="11.43">
<attribute name="NAME" value="AUX-4" layer="95" x="99.356" y="36.803" size="1.628" align="top-left"/>
</instance>
<instance part="AUX-3" gate="PART_1" x="102.87" y="-31.75">
<attribute name="NAME" value="AUX-3" layer="95" x="99.393" y="-19.077" size="1.628" align="top-left"/>
</instance>
<instance part="AUX-2" gate="PART_1" x="102.87" y="-66.04">
<attribute name="NAME" value="AUX-2" layer="95" x="99.393" y="-50.827" size="1.628" align="top-left"/>
</instance>
<instance part="TMC_X" gate="PART_1" x="-8.89" y="-31.75">
<attribute name="NAME" value="TMC_X" layer="95" x="-12.674" y="-24.157" size="1.628" align="top-left"/>
</instance>
<instance part="TMC_Y" gate="PART_1" x="-8.89" y="-45.72">
<attribute name="NAME" value="TMC_Y" layer="95" x="-12.746" y="-38.127" size="1.628" align="top-left"/>
</instance>
<instance part="TMC_Z0" gate="PART_1" x="-8.89" y="-59.69">
<attribute name="NAME" value="TMC_Z0" layer="95" x="-13.42" y="-52.097" size="1.628" align="top-left"/>
</instance>
<instance part="TMC_E" gate="PART_1" x="-8.89" y="-73.66">
<attribute name="NAME" value="TMC_E" layer="95" x="-12.637" y="-66.067" size="1.628" align="top-left"/>
</instance>
<instance part="TMC_Z1" gate="PART_1" x="-8.89" y="-87.63">
<attribute name="NAME" value="TMC_Z1" layer="95" x="-13.092" y="-80.037" size="1.628" align="top-left"/>
</instance>
<instance part="ZProbe" gate="PART_1" x="-8.89" y="52.07">
<attribute name="NAME" value="ZProbe" layer="95" x="-12.728" y="58.393" size="1.628" align="top-left"/>
</instance>
<instance part="ZMin" gate="PART_1" x="102.87" y="50.8">
<attribute name="NAME" value="ZMin" layer="95" x="100.597" y="55.853" size="1.628" align="top-left"/>
</instance>
<instance part="R1" gate="PART_1" x="25.4" y="54.61">
<attribute name="NAME" value="R1" layer="95" x="24.471" y="58.623" size="1.628" align="top-left"/>
<attribute name="VALUE" value="20K" layer="96" x="23.398" y="53.11" size="1.628" align="top-left"/>
</instance>
<instance part="R2" gate="PART_1" x="53.34" y="54.61">
<attribute name="NAME" value="R2" layer="95" x="52.084" y="58.623" size="1.628" align="top-left"/>
<attribute name="VALUE" value="10K" layer="96" x="51.666" y="53.11" size="1.628" align="top-left"/>
</instance>
<instance part="12V" gate="PART_1" x="-8.89" y="67.31">
<attribute name="NAME" value="12V" layer="95" x="-10.637" y="72.363" size="1.628" align="top-left"/>
</instance>
<instance part="NetPort1" gate="PART_1" x="2.54" y="44.704">
<attribute name="NAME" value="GND" layer="95" x="-1.878" y="42.666" size="1.628" align="top-left" rot="R90"/>
</instance>
<instance part="NetPort2" gate="PART_1" x="7.62" y="-17.526">
<attribute name="NAME" value="GND" layer="95" x="3.202" y="-19.564" size="1.628" align="top-left" rot="R90"/>
</instance>
<instance part="NetPort3" gate="PART_1" x="68.58" y="-42.926">
<attribute name="NAME" value="GND" layer="95" x="64.162" y="-44.964" size="1.628" align="top-left" rot="R90"/>
</instance>
<instance part="NetPort4" gate="PART_1" x="86.36" y="-59.436">
<attribute name="NAME" value="GND" layer="95" x="81.942" y="-61.474" size="1.628" align="top-left" rot="R90"/>
</instance>
</instances>
<busses/>
<nets>
<net name="Ground" class="0">
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="19.05" x2="34.29" y2="19.05"/>
<wire layer="91" width="0.1" x1="34.29" y1="19.05" x2="34.29" y2="-38.1"/>
<wire layer="91" width="0.1" x1="68.58" y1="-38.1" x2="95.25" y2="-38.1"/>
<wire layer="91" width="0.1" x1="34.29" y1="-38.1" x2="68.58" y2="-38.1"/>
<pinref part="EXP1" gate="PART_1" pin="P9"/>
<pinref part="AUX-3" gate="PART_1" pin="P7"/>
<wire layer="91" width="0.1" x1="68.58" y1="-39.37" x2="68.58" y2="-38.1"/>
<pinref part="NetPort3" gate="PART_1" pin="GND"/>
<junction x="68.58" y="-38.1"/>
</segment>
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="-12.7" x2="7.62" y2="-12.7"/>
<wire layer="91" width="0.1" x1="7.62" y1="-12.7" x2="57.15" y2="-12.7"/>
<wire layer="91" width="0.1" x1="57.15" y1="-12.7" x2="57.15" y2="30.48"/>
<wire layer="91" width="0.1" x1="57.15" y1="30.48" x2="95.25" y2="30.48"/>
<pinref part="EXP2" gate="PART_1" pin="P9"/>
<pinref part="AUX-4" gate="PART_1" pin="P2"/>
<wire layer="91" width="0.1" x1="7.62" y1="-13.97" x2="7.62" y2="-12.7"/>
<pinref part="NetPort2" gate="PART_1" pin="GND"/>
<junction x="7.62" y="-12.7"/>
</segment>
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="66.04" x2="2.54" y2="66.04"/>
<wire layer="91" width="0.1" x1="2.54" y1="66.04" x2="2.54" y2="49.53"/>
<wire layer="91" width="0.1" x1="2.54" y1="49.53" x2="-1.27" y2="49.53"/>
<pinref part="12V" gate="PART_1" pin="P2"/>
<pinref part="ZProbe" gate="PART_1" pin="P3"/>
<wire layer="91" width="0.1" x1="63.5" y1="49.53" x2="95.25" y2="49.53"/>
<wire layer="91" width="0.1" x1="2.54" y1="49.53" x2="63.5" y2="49.53"/>
<pinref part="ZMin" gate="PART_1" pin="P2"/>
<junction x="2.54" y="49.53"/>
<wire layer="91" width="0.1" x1="59.69" y1="54.61" x2="63.5" y2="54.61"/>
<wire layer="91" width="0.1" x1="63.5" y1="54.61" x2="63.5" y2="49.53"/>
<pinref part="R2" gate="PART_1" pin="B"/>
<junction x="63.5" y="49.53"/>
<wire layer="91" width="0.1" x1="2.54" y1="48.26" x2="2.54" y2="49.53"/>
<pinref part="NetPort1" gate="PART_1" pin="GND"/>
<junction x="2.54" y="49.53"/>
</segment>
<segment>
<wire layer="91" width="0.1" x1="86.36" y1="-55.88" x2="86.36" y2="-52.07"/>
<wire layer="91" width="0.1" x1="86.36" y1="-52.07" x2="91.44" y2="-52.07"/>
<wire layer="91" width="0.1" x1="91.44" y1="-52.07" x2="91.44" y2="-57.15"/>
<wire layer="91" width="0.1" x1="91.44" y1="-57.15" x2="95.25" y2="-57.15"/>
<pinref part="NetPort4" gate="PART_1" pin="GND"/>
<pinref part="AUX-2" gate="PART_1" pin="P2"/>
</segment>
</net>
<net name="MISO/SDO" class="0">
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="7.62" x2="17.78" y2="7.62"/>
<wire layer="91" width="0.1" x1="17.78" y1="7.62" x2="17.78" y2="-27.94"/>
<wire layer="91" width="0.1" x1="17.78" y1="-27.94" x2="95.25" y2="-27.94"/>
<pinref part="EXP2" gate="PART_1" pin="P1"/>
<pinref part="AUX-3" gate="PART_1" pin="P3"/>
<wire layer="91" width="0.1" x1="-1.27" y1="-27.94" x2="17.78" y2="-27.94"/>
<pinref part="TMC_X" gate="PART_1" pin="P1"/>
<junction x="17.78" y="-27.94"/>
<wire layer="91" width="0.1" x1="-1.27" y1="-41.91" x2="17.78" y2="-41.91"/>
<wire layer="91" width="0.1" x1="17.78" y1="-41.91" x2="17.78" y2="-27.94"/>
<pinref part="TMC_Y" gate="PART_1" pin="P1"/>
<junction x="17.78" y="-27.94"/>
<wire layer="91" width="0.1" x1="-1.27" y1="-55.88" x2="17.78" y2="-55.88"/>
<wire layer="91" width="0.1" x1="17.78" y1="-55.88" x2="17.78" y2="-41.91"/>
<pinref part="TMC_Z0" gate="PART_1" pin="P1"/>
<junction x="17.78" y="-41.91"/>
<wire layer="91" width="0.1" x1="-1.27" y1="-69.85" x2="17.78" y2="-69.85"/>
<wire layer="91" width="0.1" x1="17.78" y1="-69.85" x2="17.78" y2="-55.88"/>
<pinref part="TMC_E" gate="PART_1" pin="P1"/>
<junction x="17.78" y="-55.88"/>
<wire layer="91" width="0.1" x1="-1.27" y1="-83.82" x2="17.78" y2="-83.82"/>
<wire layer="91" width="0.1" x1="17.78" y1="-83.82" x2="17.78" y2="-69.85"/>
<pinref part="TMC_Z1" gate="PART_1" pin="P1"/>
<junction x="17.78" y="-69.85"/>
<label x="13.079" y="8.11" size="0.987" layer="95" font="vector" ratio="10" rot="R0"/>
</segment>
</net>
<net name="MOSI/SDI" class="0">
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="-5.08" x2="25.4" y2="-5.08"/>
<wire layer="91" width="0.1" x1="25.4" y1="-5.08" x2="25.4" y2="-30.48"/>
<wire layer="91" width="0.1" x1="25.4" y1="-30.48" x2="95.25" y2="-30.48"/>
<pinref part="EXP2" gate="PART_1" pin="P6"/>
<pinref part="AUX-3" gate="PART_1" pin="P4"/>
<wire layer="91" width="0.1" x1="-1.27" y1="-35.56" x2="25.4" y2="-35.56"/>
<wire layer="91" width="0.1" x1="25.4" y1="-35.56" x2="25.4" y2="-30.48"/>
<pinref part="TMC_X" gate="PART_1" pin="P4"/>
<junction x="25.4" y="-30.48"/>
<wire layer="91" width="0.1" x1="-1.27" y1="-49.53" x2="25.4" y2="-49.53"/>
<wire layer="91" width="0.1" x1="25.4" y1="-49.53" x2="25.4" y2="-35.56"/>
<pinref part="TMC_Y" gate="PART_1" pin="P4"/>
<junction x="25.4" y="-35.56"/>
<wire layer="91" width="0.1" x1="-1.27" y1="-63.5" x2="25.4" y2="-63.5"/>
<wire layer="91" width="0.1" x1="25.4" y1="-63.5" x2="25.4" y2="-49.53"/>
<pinref part="TMC_Z0" gate="PART_1" pin="P4"/>
<junction x="25.4" y="-49.53"/>
<wire layer="91" width="0.1" x1="-1.27" y1="-77.47" x2="25.4" y2="-77.47"/>
<wire layer="91" width="0.1" x1="25.4" y1="-77.47" x2="25.4" y2="-63.5"/>
<pinref part="TMC_E" gate="PART_1" pin="P4"/>
<junction x="25.4" y="-63.5"/>
<wire layer="91" width="0.1" x1="-1.27" y1="-91.44" x2="25.4" y2="-91.44"/>
<wire layer="91" width="0.1" x1="25.4" y1="-91.44" x2="25.4" y2="-77.47"/>
<pinref part="TMC_Z1" gate="PART_1" pin="P4"/>
<junction x="25.4" y="-77.47"/>
<label x="22.257" y="-4.561" size="0.987" layer="95" font="vector" ratio="10" rot="R0"/>
</segment>
</net>
<net name="Net_0" class="0">
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="39.37" x2="91.44" y2="39.37"/>
<wire layer="91" width="0.1" x1="91.44" y1="39.37" x2="91.44" y2="12.7"/>
<wire layer="91" width="0.1" x1="91.44" y1="12.7" x2="95.25" y2="12.7"/>
<pinref part="EXP1" gate="PART_1" pin="P1"/>
<pinref part="AUX-4" gate="PART_1" pin="P9"/>
</segment>
</net>
<net name="Net_1" class="0">
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="36.83" x2="90.17" y2="36.83"/>
<wire layer="91" width="0.1" x1="90.17" y1="36.83" x2="90.17" y2="10.16"/>
<wire layer="91" width="0.1" x1="90.17" y1="10.16" x2="95.25" y2="10.16"/>
<pinref part="EXP1" gate="PART_1" pin="P2"/>
<pinref part="AUX-4" gate="PART_1" pin="P10"/>
</segment>
</net>
<net name="Net_2" class="0">
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="34.29" x2="87.63" y2="34.29"/>
<wire layer="91" width="0.1" x1="87.63" y1="34.29" x2="87.63" y2="-7.62"/>
<wire layer="91" width="0.1" x1="87.63" y1="-7.62" x2="95.25" y2="-7.62"/>
<pinref part="EXP1" gate="PART_1" pin="P3"/>
<pinref part="AUX-4" gate="PART_1" pin="P17"/>
</segment>
</net>
<net name="Net_3" class="0">
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="31.75" x2="85.09" y2="31.75"/>
<wire layer="91" width="0.1" x1="85.09" y1="31.75" x2="85.09" y2="-10.16"/>
<wire layer="91" width="0.1" x1="85.09" y1="-10.16" x2="95.25" y2="-10.16"/>
<pinref part="EXP1" gate="PART_1" pin="P4"/>
<pinref part="AUX-4" gate="PART_1" pin="P18"/>
</segment>
</net>
<net name="Net_4" class="0">
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="29.21" x2="81.28" y2="29.21"/>
<wire layer="91" width="0.1" x1="81.28" y1="29.21" x2="81.28" y2="-5.08"/>
<wire layer="91" width="0.1" x1="81.28" y1="-5.08" x2="95.25" y2="-5.08"/>
<pinref part="EXP1" gate="PART_1" pin="P5"/>
<pinref part="AUX-4" gate="PART_1" pin="P16"/>
</segment>
</net>
<net name="Net_5" class="0">
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="26.67" x2="77.47" y2="26.67"/>
<wire layer="91" width="0.1" x1="77.47" y1="26.67" x2="77.47" y2="-2.54"/>
<wire layer="91" width="0.1" x1="77.47" y1="-2.54" x2="95.25" y2="-2.54"/>
<pinref part="EXP1" gate="PART_1" pin="P6"/>
<pinref part="AUX-4" gate="PART_1" pin="P15"/>
</segment>
</net>
<net name="Net_6" class="0">
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="24.13" x2="73.66" y2="24.13"/>
<wire layer="91" width="0.1" x1="73.66" y1="24.13" x2="73.66" y2="0"/>
<wire layer="91" width="0.1" x1="73.66" y1="0" x2="95.25" y2="0"/>
<pinref part="EXP1" gate="PART_1" pin="P7"/>
<pinref part="AUX-4" gate="PART_1" pin="P14"/>
</segment>
</net>
<net name="Net_7" class="0">
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="21.59" x2="69.85" y2="21.59"/>
<wire layer="91" width="0.1" x1="69.85" y1="21.59" x2="69.85" y2="2.54"/>
<wire layer="91" width="0.1" x1="69.85" y1="2.54" x2="95.25" y2="2.54"/>
<pinref part="EXP1" gate="PART_1" pin="P8"/>
<pinref part="AUX-4" gate="PART_1" pin="P13"/>
</segment>
</net>
<net name="5V" class="1">
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="16.51" x2="30.48" y2="16.51"/>
<wire layer="91" width="0.1" x1="30.48" y1="16.51" x2="30.48" y2="-22.86"/>
<wire layer="91" width="0.1" x1="30.48" y1="-22.86" x2="95.25" y2="-22.86"/>
<pinref part="EXP1" gate="PART_1" pin="P10"/>
<pinref part="AUX-3" gate="PART_1" pin="P1"/>
<label x="67.368" y="-22.37" size="0.987" layer="95" font="vector" ratio="10" rot="R0"/>
</segment>
</net>
<net name="Net_12" class="0">
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="2.54" x2="53.34" y2="2.54"/>
<wire layer="91" width="0.1" x1="53.34" y1="2.54" x2="53.34" y2="5.08"/>
<wire layer="91" width="0.1" x1="53.34" y1="5.08" x2="95.25" y2="5.08"/>
<pinref part="EXP2" gate="PART_1" pin="P3"/>
<pinref part="AUX-4" gate="PART_1" pin="P12"/>
</segment>
</net>
<net name="Net_13" class="0">
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="0" x2="38.1" y2="0"/>
<wire layer="91" width="0.1" x1="38.1" y1="0" x2="38.1" y2="-35.56"/>
<wire layer="91" width="0.1" x1="38.1" y1="-35.56" x2="95.25" y2="-35.56"/>
<pinref part="EXP2" gate="PART_1" pin="P4"/>
<pinref part="AUX-3" gate="PART_1" pin="P6"/>
</segment>
</net>
<net name="Net_14" class="0">
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="-2.54" x2="66.04" y2="-2.54"/>
<wire layer="91" width="0.1" x1="66.04" y1="-2.54" x2="66.04" y2="7.62"/>
<wire layer="91" width="0.1" x1="66.04" y1="7.62" x2="95.25" y2="7.62"/>
<pinref part="EXP2" gate="PART_1" pin="P5"/>
<pinref part="AUX-4" gate="PART_1" pin="P11"/>
</segment>
</net>
<net name="Net_16" class="0">
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="-7.62" x2="41.91" y2="-7.62"/>
<wire layer="91" width="0.1" x1="41.91" y1="-7.62" x2="41.91" y2="-25.4"/>
<wire layer="91" width="0.1" x1="41.91" y1="-25.4" x2="95.25" y2="-25.4"/>
<pinref part="EXP2" gate="PART_1" pin="P7"/>
<pinref part="AUX-3" gate="PART_1" pin="P2"/>
</segment>
</net>
<net name="Net_17" class="0">
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="-10.16" x2="60.96" y2="-10.16"/>
<wire layer="91" width="0.1" x1="60.96" y1="-10.16" x2="60.96" y2="17.78"/>
<wire layer="91" width="0.1" x1="60.96" y1="17.78" x2="95.25" y2="17.78"/>
<pinref part="EXP2" gate="PART_1" pin="P8"/>
<pinref part="AUX-4" gate="PART_1" pin="P7"/>
</segment>
</net>
<net name="Net_19" class="0">
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="-30.48" x2="13.97" y2="-30.48"/>
<wire layer="91" width="0.1" x1="13.97" y1="-30.48" x2="13.97" y2="-64.77"/>
<wire layer="91" width="0.1" x1="13.97" y1="-64.77" x2="95.25" y2="-64.77"/>
<pinref part="TMC_X" gate="PART_1" pin="P2"/>
<pinref part="AUX-2" gate="PART_1" pin="P5"/>
</segment>
</net>
<net name="Net_20" class="0">
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="-44.45" x2="34.29" y2="-44.45"/>
<wire layer="91" width="0.1" x1="34.29" y1="-44.45" x2="34.29" y2="-62.23"/>
<wire layer="91" width="0.1" x1="34.29" y1="-62.23" x2="95.25" y2="-62.23"/>
<pinref part="TMC_Y" gate="PART_1" pin="P2"/>
<pinref part="AUX-2" gate="PART_1" pin="P4"/>
</segment>
</net>
<net name="Net_21" class="0">
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="-58.42" x2="30.48" y2="-58.42"/>
<wire layer="91" width="0.1" x1="30.48" y1="-58.42" x2="30.48" y2="-67.31"/>
<wire layer="91" width="0.1" x1="30.48" y1="-67.31" x2="95.25" y2="-67.31"/>
<pinref part="TMC_Z0" gate="PART_1" pin="P2"/>
<pinref part="AUX-2" gate="PART_1" pin="P6"/>
</segment>
</net>
<net name="Net_22" class="0">
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="-72.39" x2="95.25" y2="-72.39"/>
<pinref part="TMC_E" gate="PART_1" pin="P2"/>
<pinref part="AUX-2" gate="PART_1" pin="P8"/>
</segment>
</net>
<net name="Net_23" class="0">
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="-86.36" x2="30.48" y2="-86.36"/>
<wire layer="91" width="0.1" x1="30.48" y1="-86.36" x2="30.48" y2="-69.85"/>
<wire layer="91" width="0.1" x1="30.48" y1="-69.85" x2="95.25" y2="-69.85"/>
<pinref part="TMC_Z1" gate="PART_1" pin="P2"/>
<pinref part="AUX-2" gate="PART_1" pin="P7"/>
</segment>
</net>
<net name="Net_25" class="0">
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="68.58" x2="3.81" y2="68.58"/>
<wire layer="91" width="0.1" x1="3.81" y1="68.58" x2="3.81" y2="54.61"/>
<wire layer="91" width="0.1" x1="3.81" y1="54.61" x2="-1.27" y2="54.61"/>
<pinref part="12V" gate="PART_1" pin="P1"/>
<pinref part="ZProbe" gate="PART_1" pin="P1"/>
</segment>
</net>
<net name="Net_26" class="0">
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="52.07" x2="15.24" y2="52.07"/>
<wire layer="91" width="0.1" x1="15.24" y1="52.07" x2="15.24" y2="54.61"/>
<wire layer="91" width="0.1" x1="15.24" y1="54.61" x2="19.05" y2="54.61"/>
<pinref part="ZProbe" gate="PART_1" pin="P2"/>
<pinref part="R1" gate="PART_1" pin="A"/>
</segment>
</net>
<net name="Net_27" class="0">
<segment>
<wire layer="91" width="0.1" x1="39.37" y1="54.61" x2="46.99" y2="54.61"/>
<wire layer="91" width="0.1" x1="31.75" y1="54.61" x2="39.37" y2="54.61"/>
<pinref part="R1" gate="PART_1" pin="B"/>
<pinref part="R2" gate="PART_1" pin="A"/>
<wire layer="91" width="0.1" x1="95.25" y1="52.07" x2="67.31" y2="52.07"/>
<wire layer="91" width="0.1" x1="67.31" y1="52.07" x2="67.31" y2="60.96"/>
<wire layer="91" width="0.1" x1="67.31" y1="60.96" x2="39.37" y2="60.96"/>
<wire layer="91" width="0.1" x1="39.37" y1="60.96" x2="39.37" y2="54.61"/>
<pinref part="ZMin" gate="PART_1" pin="P1"/>
<junction x="39.37" y="54.61"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<wire layer="91" width="0.1" x1="-1.27" y1="5.08" x2="21.59" y2="5.08"/>
<wire layer="91" width="0.1" x1="21.59" y1="5.08" x2="21.59" y2="-33.02"/>
<wire layer="91" width="0.1" x1="21.59" y1="-33.02" x2="95.25" y2="-33.02"/>
<pinref part="EXP2" gate="PART_1" pin="P2"/>
<pinref part="AUX-3" gate="PART_1" pin="P5"/>
<wire layer="91" width="0.1" x1="-1.27" y1="-33.02" x2="21.59" y2="-33.02"/>
<pinref part="TMC_X" gate="PART_1" pin="P3"/>
<junction x="21.59" y="-33.02"/>
<wire layer="91" width="0.1" x1="-1.27" y1="-46.99" x2="21.59" y2="-46.99"/>
<wire layer="91" width="0.1" x1="21.59" y1="-46.99" x2="21.59" y2="-33.02"/>
<pinref part="TMC_Y" gate="PART_1" pin="P3"/>
<junction x="21.59" y="-33.02"/>
<wire layer="91" width="0.1" x1="-1.27" y1="-60.96" x2="21.59" y2="-60.96"/>
<wire layer="91" width="0.1" x1="21.59" y1="-60.96" x2="21.59" y2="-46.99"/>
<pinref part="TMC_Z0" gate="PART_1" pin="P3"/>
<junction x="21.59" y="-46.99"/>
<wire layer="91" width="0.1" x1="-1.27" y1="-74.93" x2="21.59" y2="-74.93"/>
<wire layer="91" width="0.1" x1="21.59" y1="-74.93" x2="21.59" y2="-60.96"/>
<pinref part="TMC_E" gate="PART_1" pin="P3"/>
<junction x="21.59" y="-60.96"/>
<wire layer="91" width="0.1" x1="-1.27" y1="-88.9" x2="21.59" y2="-88.9"/>
<wire layer="91" width="0.1" x1="21.59" y1="-88.9" x2="21.59" y2="-74.93"/>
<pinref part="TMC_Z1" gate="PART_1" pin="P3"/>
<junction x="21.59" y="-74.93"/>
<label x="19.717" y="5.944" size="0.987" layer="95" font="vector" ratio="10" rot="R0"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>

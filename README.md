TMC2130 Silent Step Stick RAMPS Adapter
=======================================

This was converted from the original DipTrace files at https://www.thingiverse.com/thing:3337519
to EAGLE files, which were then imported into KiCad and cleaned up a little (PCB zones filled and
labels made visible).
